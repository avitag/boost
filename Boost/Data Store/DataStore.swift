//
//  DataStore.swift
//  Boost
//
//  Created by Avinash  Tag on 28/10/2019.
//  Copyright © 2019 Avinash Tag. All rights reserved.
//

import Foundation


class DataStore: NSObject {
    
    static let shared: DataStore = DataStore()

    
    required override init() {
        super.init()
        
        if !(FileManager.default.fileExists(atPath: DataStore.path.absoluteString)){
            makeCopy(ofResource: "data", ofType: "json")
        }
    }
    
    static var  path : URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        return documentsURL!.appendingPathComponent("data").appendingPathExtension("json")
    }

    func makeCopy(ofResource filename: String, ofType ext: String) {

        guard let source = Bundle.main.url(forResource: filename, withExtension: ext) else { return }
            let fileManager = FileManager.default
            do {
                try fileManager.copyItem(at: source, to: DataStore.path)
            } catch let e{
                print(e.localizedDescription)
            }
    }
}
