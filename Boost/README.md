# Boost
Access the contact list from given json.data file, user can create and edit contact info.


## Requirements

* [Xcode 11](https://developer.apple.com/xcode/)
* [Swift 5](https://github.com/apple/swift).


## How to build

Clone the project in your machine using ``https://github.com/avinashtag/Boost``

Go to the project directory **Project Directory -> Boost**

The project can be run using Xcode 11 and built/tested using the standard Xcode build (⌘B) and test (⌘U) commands.

## Class Diagram

Clone the project in your machine using ``https://github.com/avinashtag/Boost``
Run **diagram.html** in htmltemplate folder


## Design Pattern
* `Based on VIPER - Design Pattern `
Clean iOS Architecture based on VIPER


## Features
* `Swift Programming Language - version 5`
* `Design driven development`  
* `Custom UIView`
* `VIPER - Design Pattern `
* `Protocol-Oriented Programming `
#
● Home screen:
- Showing contacts from data.json file with an add button on top right corener.
- Pull to refresh to update the Contacts.
- Search contact
- Name index to navigate in contact list faster and convinient 

● Contact Detail Screen:  where the user can see the details of contact
- Edit Contact.

● Add Contact Screen:  where the user can add a new contact
- Add/Create Contact.


● Compatible with iOS 12.0+ or above.

● Unit tests added .

