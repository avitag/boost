//
//  Date.swift
//  Boost
//
//  Created by Avinash  Tag on 28/10/2019.
//  Copyright © 2019 Avinash Tag. All rights reserved.
//

import Foundation

extension Date{
    
    func string(format:String!) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = Locale(identifier: "en_US_POSIX")
        let date = formatter.string(from: self)
        return date
    }
}
