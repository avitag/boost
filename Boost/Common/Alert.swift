//
//  Alert.swift
//  Boost
//
//  Created by Avinash  Tag on 28/10/2019.
//  Copyright © 2019 Avinash Tag. All rights reserved.
//

import Foundation
import UIKit


protocol Alert {
    func present(error message: String, completion: ( ()-> Void)? )
    func present(success message: String, completion: ( ()-> Void)? )
}

extension Alert where Self: UIViewController{
    
    func present(error message: String, completion: ( ()-> Void)? ) {
        DispatchQueue.main.async {
            UINotificationFeedbackGenerator().notificationOccurred(.error)
            self.presentAlert(title: "Error".localized(), message: message, options: "Okay".localized()) { (index) in
                completion?()
            }
        }
    }

    func present(success message: String, completion: ( ()-> Void)? ) {
        DispatchQueue.main.async {
            UINotificationFeedbackGenerator().notificationOccurred(.success)
            self.presentAlert(title: "Success".localized(), message: message, options: "Okay".localized()) { (index) in
                completion?()
            }
        }
    }
    
    func present(confirmation message: String, completion: @escaping ( (Int)-> Void)) {
        DispatchQueue.main.async {
            UINotificationFeedbackGenerator().notificationOccurred(.success)
            self.presentAlert(title: "Confirmation".localized(), message: message, options: "Cancel".localized(), "Yes".localized(), completion: completion)
        }
    }

    
    func presentAlert(title: String, message: String, options: String..., completion: @escaping (Int) -> Void) {
        self.view.endEditing(true)
        print(Thread.isMainThread)
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, option) in options.enumerated() {
            alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
                completion(index)
            }))
        }
        self.present(alertController, animated: true, completion: nil)
    }

}
