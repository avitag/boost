//
//  UIImage.swift
//  Boost
//
//  Created by Avinash  Tag on 28/10/2019.
//  Copyright © 2019 Avinash Tag. All rights reserved.
//

import Foundation
import Photos
import UIKit

extension UIImage{
        
    
    public convenience init( intials: String, size: CGSize, textColor: UIColor? = .white, backgroundColor: UIColor? = .gray) {

        let sizeThrice = CGSize(width: size.height*3.0, height: size.width*3.0)
        let name = UILabel()
        name.frame.size = size
        name.textColor = textColor
        name.font = UIFont.systemFont(ofSize: 20)
        name.text = intials
        name.textAlignment = .center
        name.backgroundColor = backgroundColor
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        name.layer.render(in: UIGraphicsGetCurrentContext()!)
       
        var image = UIGraphicsGetImageFromCurrentImageContext()
        
        let rect = CGRect(origin:CGPoint(x: 0, y: 0), size: size)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        UIBezierPath(
            roundedRect: rect,
            cornerRadius: size.height
            ).addClip()
        image?.draw(in: rect)
        image = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()

        if let image = image {
            self.init(cgImage: image.cgImage!)
//            self.init(cgImage: image.cgImage!, scale: image.scale, orientation: image.imageOrientation)
        } else {
            self.init()
        }
    }
    
    func scaleImageAZUpload() -> UIImage?  {

        guard self.size.width > 1024 else {return self}
        let newWidth: CGFloat = 1024.0
        let scale = newWidth / self.size.width
        let newHeight = self.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage

    }
    
    
    func resize(asset: PHAsset, completion: ((_ image: UIImage?)->Void)?){
        
            let options = PHImageRequestOptions()
            options.isSynchronous = true
            var widthSize : CGFloat = 1000
            var heightSize : CGFloat = 1000
            let maxSize : CGFloat = 4000.0
            
            if (self.size.width > widthSize) {
                widthSize = self.size.width
            }else{
                widthSize = self.size.width
            }
            
            if (self.size.height > heightSize) {
                heightSize = self.size.height
            }else{
                heightSize = self.size.height
            }
            
            PHImageManager.default().requestImage(for: asset, targetSize: CGSize(width:widthSize, height: heightSize), contentMode: .default, options: options, resultHandler: { (result, info) in
                completion?(result)
            })
    }
}
