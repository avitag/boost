//
//  Error.swift
//  Boost
//
//  Created by Avinash  Tag on 28/10/2019.
//  Copyright © 2019 Avinash Tag. All rights reserved.
//

import Foundation

extension Error{
    
    func code() -> Int {
        return (self as NSError).code
    }
}
