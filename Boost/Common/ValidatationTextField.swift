//
//  ValidatationTextField.swift
//  AestaZeneca
//
//  Created by Avinash Tag on 23/01/2019.
//  Copyright © 2019 UCrest. All rights reserved.
//

import UIKit



internal struct Validation {
    
    
    typealias Result = (() -> Bool)
    var result: Result
    var failureMessage: String
    
    init(error: String = "", withResult result: @escaping Result) {
        self.result = result
        self.failureMessage = error
    }
}



class ValidatationTextField: UITextField {

    fileprivate var validation  : [Validation] = []
    private var errorLabel : UILabel = UILabel(frame: .zero)
    private var titleLabel : UILabel = UILabel(frame: .zero)
    private var mandatory : String?
    var maxLength: Int?
    var allowedCharacters: CharacterSet?

    
    @IBInspectable var errorFont: UIFont? {
        set {
            self.errorLabel.font = newValue
        }
        get {
            return self.errorLabel.font
        }
    }
   
    @IBInspectable var errorTextColor: UIColor? {
        set {
            self.errorLabel.textColor = newValue
        }
        get {
            return self.errorLabel.textColor
        }
    }

    
    @IBInspectable var titleText: String? {
        set {
            self.titleLabel.text = newValue
        }
        get {
            return self.titleLabel.text
        }
    }
    
    @IBInspectable var titleFont: CGFloat {
        set {
            self.titleLabel.font = UIFont.systemFont(ofSize: newValue)
        }
        get {
            return self.titleLabel.font.pointSize
        }
    }
    
    @IBInspectable var titleTextColor: UIColor? {
        set {
            self.titleLabel.textColor = newValue
        }
        get {
            return self.titleLabel.textColor
        }
    }
    
    @IBInspectable var mandate: String? {
        set {
            self.mandatory = newValue
            error(message: mandatory!) { () -> Bool in
                return self.text?.isBlank() ?? true
            }
        }
        get {
            return self.mandatory
        }
    }
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        errorLabel.font = UIFont.systemFont(ofSize: CGFloat(11))
        errorLabel.translatesAutoresizingMaskIntoConstraints = false
        errorLabel.text = ""
        errorLabel.textAlignment = .right
        errorLabel.textColor = .red
        errorLabel.numberOfLines = 2
        errorLabel.adjustsFontSizeToFitWidth = true
        
        // Add Error label on view
        self.addSubview(errorLabel)
        errorLabel.topAnchor.constraint(equalTo: self.bottomAnchor, constant:2).isActive = true
        errorLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        errorLabel.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true


        titleLabel.font = self.font
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textAlignment = .left
        titleLabel.textColor = .black
        titleLabel.numberOfLines = 2
        titleLabel.adjustsFontSizeToFitWidth = true
        
        // Add Error label on view
        self.addSubview(titleLabel)
        titleLabel.bottomAnchor.constraint(equalTo: self.topAnchor, constant:-2.0).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        titleLabel.isHidden  = true
        
    }
    
    func error(message: String, condition: @escaping Validation.Result)  {

        validation.append(Validation(error: message, withResult: condition))

        
    }

    func decorate() {
        titleLabel.isHidden = titleLabel.text == nil
    }
    
    func validateTitle()  {
        
        guard !(self.text?.isEmpty ?? true) else{
            titleLabel.isHidden = true
            return
        }
        decorate()
    }
    func validate() -> Bool {
        
        self.validateTitle()
        for valid in validation {
            self.errorLabel.text = ""
            if valid.result(){
                self.errorLabel.text = valid.failureMessage
                print("error: "+valid.failureMessage)
                return false
            }
        }
        return true
    }
    func clear()  {
        self.text = ""
        self.titleLabel.isHidden = true
        self.errorLabel.text = ""
    }
    func setText(_ text: String?){
        self.text = text
        if text?.isEmpty == false{
            self.titleLabel.isHidden = false
        }
    }

    func textFieldValidation( range: NSRange, string: String) -> Bool
    {
        guard range.length == 0 else{return true}
        
        guard (self.allowedCharacters?.isSuperset(of: CharacterSet(charactersIn: string))) == true else { return false}
        guard range.length + 1 + range.location <= self.maxLength ?? 0 else { return false}
        return true
    }

}

