var entities = [{
  "id": 1,
  "typeString": "class",
  "properties": [
    {
  "name": "var contactListWorker : ContactListWorker?",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "methods": [
    {
  "name": "setUp()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "tearDown()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "testExample()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "testPerformanceExample()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "testContacts()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "testSaveContact()",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "BoostTests",
  "superClass": 86
},{
  "id": 2,
  "typeString": "protocol",
  "properties": [
    {
  "name": "var contacts: ContactList.ViewModel.Data?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var filteredContacts: ContactList.ViewModel.Data?",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ContactListViewDataStore"
},{
  "id": 3,
  "typeString": "protocol",
  "methods": [
    {
  "name": "display(contacts viewModel: ContactList.ViewModel.Data)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "display(error: ParseError)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ContactListDisplayLogic",
  "superClass": 87
},{
  "id": 4,
  "typeString": "class",
  "properties": [
    {
  "name": "var contactList: UITableView!",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let refreshControl",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var contacts: ContactList.ViewModel.Data?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var filteredContacts: ContactList.ViewModel.Data?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var tiles: [String]?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let searchController",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var interactor: ContactListBusinessLogic?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var router: (NSObjectProtocol & ContactListRoutingLogic & ContactListDataPassing)? required @objc",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "methods": [
    {
  "name": "setup()",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "prepare(for segue: UIStoryboardSegue, sender: Any?)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "viewDidLoad()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "viewDidAppear(_ animated: Bool)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "initialise()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "refreshContacts(_ sender: Any?)",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "init?(coder aDecoder: NSCoder)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    2
  ],
  "name": "ContactListViewController",
  "superClass": 88,
  "extensions": [
    5,
    6,
    7,
    8,
    9
  ]
},{
  "id": 10,
  "typeString": "class",
  "properties": [
    {
  "name": "var picture: UIImageView!",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var name: UILabel!",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ContactListCell",
  "superClass": 89
},{
  "id": 11,
  "typeString": "enum",
  "protocols": [
    91
  ],
  "cases": [
    {
  "name": "id"
},
    {
  "name": "firstName"
},
    {
  "name": "lastName"
},
    {
  "name": "email"
},
    {
  "name": "phone"
}
  ],
  "name": "CodingKeys",
  "superClass": 90
},{
  "id": 12,
  "typeString": "struct",
  "properties": [
    {
  "name": "var id : String",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var firstName: String",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var lastName: String",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var email: String?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var phone: String?",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "methods": [
    {
  "name": "displayName() -> String",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "initials() -> String",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "image( size: CGSize, textColor: UIColor? = .white, backgroundColor: UIColor? = .gray ) -> UIImage",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "init()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "init(from decoder: Decoder) throws",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "Contact",
  "superClass": 92,
  "containedEntities": [
    11
  ]
},{
  "id": 13,
  "typeString": "struct",
  "properties": [
    {
  "name": "var data: [String: [Contact]]",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "Data",
  "superClass": 92
},{
  "id": 14,
  "typeString": "struct",
  "properties": [
    {
  "name": "var id: String",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var name: String",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var initials: String",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "Contact",
  "superClass": 92
},{
  "id": 15,
  "typeString": "enum",
  "name": "ViewModel",
  "containedEntities": [
    13,
    14
  ]
},{
  "id": 16,
  "typeString": "enum",
  "properties": [
    {
  "name": "var lastName: String",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var email: String?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var phstName, email, phone } }",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "methods": [
    {
  "name": "displayName() -> String",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "initials() -> String",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "image( size: CGSize, textColor: UIColor? = .white, backgroundColor: UIColor? = .gray ) -> UIImage",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "init()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "init(from decoder: Decoder) throws",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ContactList",
  "containedEntities": [
    11,
    12,
    13,
    14,
    15
  ]
},{
  "id": 17,
  "typeString": "protocol",
  "methods": [
    {
  "name": "present(contacts: [ContactList.Contact])",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "present(error: ParseError)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ContactListPresentationLogic"
},{
  "id": 18,
  "typeString": "class",
  "properties": [
    {
  "name": "var viewController: ContactListDisplayLogic?",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "methods": [
    {
  "name": "present(contacts: [ContactList.Contact])",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "present(error: ParseError)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    17
  ],
  "name": "ContactListPresenter"
},{
  "id": 19,
  "typeString": "class",
  "methods": [
    {
  "name": "fetchContacts(_ completion: (ServiceResult<[ContactList.Contact]?, ParseError>) -> Void)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ContactListWorker",
  "superClass": 61
},{
  "id": 20,
  "typeString": "protocol",
  "methods": [
    {
  "name": "routeToContactDetailViewControllerWithSegue(_ segue: UIStoryboardSegue?)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ContactListRoutingLogic"
},{
  "id": 21,
  "typeString": "protocol",
  "properties": [
    {
  "name": "var dataStore: ContactListDataStore?",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ContactListDataPassing"
},{
  "id": 22,
  "typeString": "class",
  "properties": [
    {
  "name": "var viewController: ContactListViewController?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var dataStore: ContactListDataStore? required",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "methods": [
    {
  "name": "routeToContactDetailViewControllerWithSegue(_ segue: UIStoryboardSegue?)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "routeToCreateContactWithSegue(_ segue: UIStoryboardSegue?)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "passDataToContactDetail(source: ContactListDataStore, destination: inout ContactDetailDataStore)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "passDataToCreateContact(source: ContactListDataStore, destination: inout ContactDetailDataStore)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "init()",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    20
  ],
  "name": "ContactListRouter",
  "superClass": 93
},{
  "id": 23,
  "typeString": "protocol",
  "methods": [
    {
  "name": "fetchContacts()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "select( contact: ContactList.ViewModel.Contact)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ContactListBusinessLogic"
},{
  "id": 24,
  "typeString": "protocol",
  "properties": [
    {
  "name": "var contacts : [ ContactList.Contact]",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var contactSelected: ContactList.Contact?",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ContactListDataStore"
},{
  "id": 25,
  "typeString": "class",
  "properties": [
    {
  "name": "var contacts: [ContactList.Contact]",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var contactSelected: ContactList.Contact?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var presenter: ContactListPresentationLogic?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var worker: ContactListWorker?",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "methods": [
    {
  "name": "fetchContacts()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "select( contact: ContactList.ViewModel.Contact)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    23
  ],
  "name": "ContactListInteractor"
},{
  "id": 26,
  "typeString": "class",
  "properties": [
    {
  "name": "var entitle: UILabel!",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var entitledValue: ValidatationTextField!",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ContactDetailCell",
  "superClass": 89
},{
  "id": 27,
  "typeString": "protocol",
  "methods": [
    {
  "name": "display( contact: ContactList.Contact?)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "display( saveContact success: Bool, message: String)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ContactDetailDisplayLogic",
  "superClass": 87
},{
  "id": 28,
  "typeString": "class",
  "properties": [
    {
  "name": "var mainHeader: UIView!",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var topHeader: UIView!",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var tableView: UITableView!",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var contactImageview: UIImageView!",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var contact: ContactList.Contact?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var interactor: ContactDetailBusinessLogic?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var router: (NSObjectProtocol & ContactDetailRoutingLogic & ContactDetailDataPassing)? required",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "methods": [
    {
  "name": "setup()",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "prepare(for segue: UIStoryboardSegue, sender: Any?)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "viewDidLoad()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "cancel(_ sender: Any?)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "save(_ sender: Any?)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "validate() -> Bool",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "init?(coder aDecoder: NSCoder)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ContactDetailViewController",
  "superClass": 88,
  "extensions": [
    29,
    30,
    31,
    32
  ]
},{
  "id": 34,
  "typeString": "class",
  "methods": [
    {
  "name": "save(request: ContactDetail.Request, completion: (ServiceResult<Bool, ParseError>) -> Void)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ContactDetailWorker"
},{
  "id": 35,
  "typeString": "protocol",
  "name": "ContactDetailRoutingLogic"
},{
  "id": 36,
  "typeString": "protocol",
  "properties": [
    {
  "name": "var dataStore: ContactDetailDataStore?",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ContactDetailDataPassing"
},{
  "id": 37,
  "typeString": "class",
  "properties": [
    {
  "name": "var viewController: ContactDetailViewController?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var dataStore: ContactDetailDataStore?",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ContactDetailRouter",
  "superClass": 93
},{
  "id": 38,
  "typeString": "struct",
  "properties": [
    {
  "name": "var contacts: [ContactList.Contact]",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "Request",
  "superClass": 92
},{
  "id": 39,
  "typeString": "enum",
  "name": "ContactDetail",
  "containedEntities": [
    38
  ]
},{
  "id": 40,
  "typeString": "protocol",
  "methods": [
    {
  "name": "present(contact: ContactList.Contact?)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "present(success: String)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "present(error: String)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ContactDetailPresentationLogic"
},{
  "id": 41,
  "typeString": "class",
  "properties": [
    {
  "name": "var viewController: ContactDetailDisplayLogic?",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "methods": [
    {
  "name": "present(contact: ContactList.Contact?)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "present(success: String)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "present(error: String)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    40
  ],
  "name": "ContactDetailPresenter"
},{
  "id": 42,
  "typeString": "protocol",
  "methods": [
    {
  "name": "fetchContact()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "save( contact: ContactList.Contact )",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ContactDetailBusinessLogic"
},{
  "id": 43,
  "typeString": "protocol",
  "properties": [
    {
  "name": "var contact : ContactList.Contact?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var contacts : [ContactList.Contact]",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ContactDetailDataStore"
},{
  "id": 44,
  "typeString": "class",
  "properties": [
    {
  "name": "var contact: ContactList.Contact?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var contacts: [ContactList.Contact]",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var presenter: ContactDetailPresentationLogic?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var worker: ContactDetailWorker? required",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "methods": [
    {
  "name": "fetchContact()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "save( contact: ContactList.Contact )",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "init()",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    42
  ],
  "name": "ContactDetailInteractor"
},{
  "id": 48,
  "typeString": "struct",
  "properties": [
    {
  "name": "var result: Result",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var failureMessage: String",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "methods": [
    {
  "name": "init(error: String = , withResult result: @escaping Result)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "Validation"
},{
  "id": 49,
  "typeString": "class",
  "properties": [
    {
  "name": "var validation : [Validation]",
  "type": "instance",
  "accessLevel": "fileprivate"
},
    {
  "name": "var errorLabel : UILabel",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var titleLabel : UILabel",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var mandatory : String?",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var maxLength: Int?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var allowedCharacters: CharacterSet?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var errorFont: UIFont?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var errorTextColor: UIColor?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var titleText: String?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var titleFont: CGFloat",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var titleTextColor: UIColor?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var mandate: String?",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "methods": [
    {
  "name": "error(message: String, condition: @escaping Validation.Result)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "decorate()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "validateTitle()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "validate() -> Bool",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "clear()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "setText(_ text: String?)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "textFieldValidation( range: NSRange, string: String) -> Bool",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "init?(coder aDecoder: NSCoder)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ValidatationTextField",
  "superClass": 94
},{
  "id": 50,
  "typeString": "protocol",
  "methods": [
    {
  "name": "present(error message: String, completion: ( ()-> Void)? )",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "present(success message: String, completion: ( ()-> Void)? )",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "Alert",
  "extensions": [
    51
  ]
},{
  "id": 53,
  "typeString": "struct",
  "properties": [
    {
  "name": "let Okay",
  "type": "type",
  "accessLevel": "internal"
},
    {
  "name": "let Yes",
  "type": "type",
  "accessLevel": "internal"
},
    {
  "name": "let No",
  "type": "type",
  "accessLevel": "internal"
}
  ],
  "name": "Localisable",
  "extensions": [
    33,
    45
  ]
},{
  "id": 56,
  "typeString": "class",
  "properties": [
    {
  "name": "let shared: DataStore",
  "type": "type",
  "accessLevel": "internal"
},
    {
  "name": "var path : URL",
  "type": "type",
  "accessLevel": "internal"
},
    {
  "name": "let documentsURL",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "methods": [
    {
  "name": "makeCopy(ofResource filename: String, ofType ext: String)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "init()",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "DataStore",
  "superClass": 93
},{
  "id": 57,
  "typeString": "class",
  "methods": [
    {
  "name": "application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    96
  ],
  "name": "AppDelegate",
  "superClass": 95
},{
  "id": 58,
  "typeString": "enum",
  "cases": [
    {
  "name": "success"
},
    {
  "name": "failure"
}
  ],
  "name": "ServiceResult",
  "superClass": 97
},{
  "id": 59,
  "typeString": "enum",
  "properties": [
    {
  "name": "var message: String",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var code: Int",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "cases": [
    {
  "name": "jsonParsingFailure"
},
    {
  "name": "invalidData"
},
    {
  "name": "let"
},
    {
  "name": "let"
},
    {
  "name": "let"
},
    {
  "name": "let"
}
  ],
  "name": "ParseError",
  "superClass": 98
},{
  "id": 60,
  "typeString": "protocol",
  "methods": [
    {
  "name": "decode<T: Decodable>(with path: URL, decodingType: T.Type, decode: (Decodable) -> T?, completion: (ServiceResult<T?, ParseError>) -> Void)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "ParserProtocol"
},{
  "id": 61,
  "typeString": "class",
  "methods": [
    {
  "name": "decode<T: Decodable>(with path: URL, decodingType: T.Type, decode: (Decodable) -> T?, completion: (ServiceResult<T?, ParseError>) -> Void)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    60
  ],
  "name": "Parser"
},{
  "id": 62,
  "typeString": "class",
  "properties": [
    {
  "name": "var window: UIWindow?",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "methods": [
    {
  "name": "scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "sceneDidDisconnect(_ scene: UIScene)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "sceneDidBecomeActive(_ scene: UIScene)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "sceneWillResignActive(_ scene: UIScene)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "sceneWillEnterForeground(_ scene: UIScene)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "sceneDidEnterBackground(_ scene: UIScene)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "SceneDelegate",
  "superClass": 95
},{
  "id": 63,
  "typeString": "class",
  "properties": [
    {
  "name": "var privatePlaceholderLabel: UILabel?",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var placeholderLabel: UILabel",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let unwrappedPlaceholderLabel",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let label",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var placeholderTextColor : UIColor?",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "var placeholder : String?",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "var text: String!",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "var font : UIFont?",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "let unwrappedFont",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var textAlignment: NSTextAlignment",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "var delegate : UITextViewDelegate?",
  "type": "instance",
  "accessLevel": "open"
}
  ],
  "methods": [
    {
  "name": "awakeFromNib()",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "layoutSubviews()",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "refreshPlaceholder()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "init?(coder aDecoder: NSCoder)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "init(frame: CGRect, textContainer: NSTextContainer?)",
  "type": "instance",
  "accessLevel": "public"
}
  ],
  "name": "IQTextView",
  "superClass": 99
},{
  "id": 64,
  "typeString": "class",
  "name": "IQPreviousNextView",
  "superClass": 100
},{
  "id": 65,
  "typeString": "class",
  "properties": [
    {
  "name": "var titleFont : UIFont?",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "let unwrappedFont",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var title: String?",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "var titleColor : UIColor?",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "let color",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var selectableTitleColor : UIColor?",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "let color",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var invocation : IQInvocation?",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "let target",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let action",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var titleButton : UIButton?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var _titleView : UIView? @objc",
  "type": "instance",
  "accessLevel": "private"
}
  ],
  "methods": [
    {
  "name": "init()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "init(title : String?)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "init?(coder aDecoder: NSCoder)",
  "type": "instance",
  "accessLevel": "public"
}
  ],
  "name": "IQTitleBarButtonItem",
  "superClass": 66
},{
  "id": 66,
  "typeString": "class",
  "properties": [
    {
  "name": "var _classInitialize: Void",
  "type": "type",
  "accessLevel": "private"
},
    {
  "name": "var tintColor: UIColor?",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "var textAttributes",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let foregroundColorKey",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var textAttributes",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let foregroundColorKey",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var textAttributes",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let foregroundColorKey",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let attributes",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let attributes",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var isSystemItem",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var invocation : IQInvocation? deinit",
  "type": "instance",
  "accessLevel": "open"
}
  ],
  "methods": [
    {
  "name": "classInitialize()",
  "type": "type",
  "accessLevel": "private"
},
    {
  "name": "setTarget(_ target: AnyObject?, action: Selector?)",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "init()",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "init?(coder aDecoder: NSCoder)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "IQBarButtonItem",
  "superClass": 101
},{
  "id": 67,
  "typeString": "class",
  "properties": [
    {
  "name": "var _classInitialize: Void",
  "type": "type",
  "accessLevel": "private"
},
    {
  "name": "var privatePreviousBarButton: IQBarButtonItem? @objc",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var previousBarButton : IQBarButtonItem",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "var privateNextBarButton: IQBarButtonItem? @objc",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var nextBarButton : IQBarButtonItem",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "var privateTitleBarButton: IQTitleBarButtonItem? @objc",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var titleBarButton : IQTitleBarButtonItem",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "var privateDoneBarButton: IQBarButtonItem? @objc",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var doneBarButton : IQBarButtonItem",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "var privateFixedSpaceBarButton: IQBarButtonItem? @objc",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var fixedSpaceBarButton : IQBarButtonItem",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "var tintColor: UIColor!",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "let unwrappedItems",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var barStyle: UIBarStyle",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "var enableInputClicksWhenVisible: Bool",
  "type": "instance",
  "accessLevel": "open"
}
  ],
  "methods": [
    {
  "name": "classInitialize()",
  "type": "type",
  "accessLevel": "private"
},
    {
  "name": "sizeThatFits(_ size: CGSize) -> CGSize",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "layoutSubviews()",
  "type": "instance",
  "accessLevel": "open"
},
    {
  "name": "init(frame: CGRect)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "init?(coder aDecoder: NSCoder)",
  "type": "instance",
  "accessLevel": "public"
}
  ],
  "protocols": [
    103
  ],
  "name": "IQToolbar",
  "superClass": 102
},{
  "id": 68,
  "typeString": "struct",
  "properties": [
    {
  "name": "let nilButton",
  "type": "type",
  "accessLevel": "internal"
}
  ],
  "name": "Static"
},{
  "id": 70,
  "typeString": "class",
  "properties": [
    {
  "name": "var target: AnyObject?",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var action: Selector @objc @objc deinit",
  "type": "instance",
  "accessLevel": "public"
}
  ],
  "methods": [
    {
  "name": "invoke(from: Any)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "init(_ target: AnyObject, _ action: Selector)",
  "type": "instance",
  "accessLevel": "public"
}
  ],
  "name": "IQInvocation",
  "superClass": 93
},{
  "id": 71,
  "typeString": "enum",
  "cases": [
    {
  "name": "bySubviews case byTag case byPosition"
}
  ],
  "name": "IQAutoToolbarManageBehaviour",
  "superClass": 104
},{
  "id": 72,
  "typeString": "enum",
  "cases": [
    {
  "name": "Default case alwaysHide case alwaysShow"
}
  ],
  "name": "IQPreviousNextDisplayMode",
  "superClass": 104
},{
  "id": 73,
  "typeString": "enum",
  "cases": [
    {
  "name": "Default case enabled case disabled"
}
  ],
  "name": "IQEnableMode",
  "superClass": 104
},{
  "id": 74,
  "typeString": "class",
  "properties": [
    {
  "name": "var textFieldDelegate: UITextFieldDelegate?",
  "type": "instance",
  "accessLevel": "fileprivate"
},
    {
  "name": "var textViewDelegate: UITextViewDelegate?",
  "type": "instance",
  "accessLevel": "fileprivate"
},
    {
  "name": "var textFieldView: UIView?",
  "type": "instance",
  "accessLevel": "fileprivate"
},
    {
  "name": "var originalReturnKeyType",
  "type": "instance",
  "accessLevel": "fileprivate"
}
  ],
  "methods": [
    {
  "name": "init(textFieldView : UIView?, textFieldDelegate : UITextFieldDelegate?, textViewDelegate : UITextViewDelegate?, originalReturnKeyType : UIReturnKeyType = .default)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "IQTextFieldViewInfoModal",
  "superClass": 93
},{
  "id": 75,
  "typeString": "class",
  "properties": [
    {
  "name": "var delegate: (UITextFieldDelegate & UITextViewDelegate)? @objc",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var lastTextFieldReturnKeyType : UIReturnKeyType",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "let view",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let textField",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let textView",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var textFieldInfoCache",
  "type": "instance",
  "accessLevel": "private"
}
  ],
  "methods": [
    {
  "name": "textFieldViewCachedInfo(_ textField : UIView) -> IQTextFieldViewInfoModal?",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "updateReturnKeyTypeOnTextField(_ view : UIView)",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "addTextFieldView(_ view : UIView)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "removeTextFieldView(_ view : UIView)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addResponderFromView(_ view : UIView)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "removeResponderFromView(_ view : UIView)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "goToNextResponderOrResign(_ view : UIView) -> Bool",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "textFieldShouldBeginEditing(_ textField: UITextField) -> Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "textFieldShouldEndEditing(_ textField: UITextField) -> Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "textFieldDidBeginEditing(_ textField: UITextField)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "textFieldDidEndEditing(_ textField: UITextField)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "textFieldShouldClear(_ textField: UITextField) -> Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "textFieldShouldReturn(_ textField: UITextField) -> Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "textViewShouldBeginEditing(_ textView: UITextView) -> Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "textViewShouldEndEditing(_ textView: UITextView) -> Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "textViewDidBeginEditing(_ textView: UITextView)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "textViewDidEndEditing(_ textView: UITextView)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "textViewDidChange(_ textView: UITextView)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "textViewDidChangeSelection(_ textView: UITextView)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "textView(_ aTextView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "textView(_ aTextView: UITextView, shouldInteractWith textAttachment: NSTextAttachment, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "textView(_ aTextView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "textView(_ aTextView: UITextView, shouldInteractWith textAttachment: NSTextAttachment, in characterRange: NSRange) -> Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "init()",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "init(controller : UIViewController)",
  "type": "instance",
  "accessLevel": "public"
}
  ],
  "name": "IQKeyboardReturnKeyHandler",
  "superClass": 93
},{
  "id": 76,
  "typeString": "struct",
  "properties": [
    {
  "name": "let kbManager",
  "type": "type",
  "accessLevel": "internal"
}
  ],
  "name": "Static"
},{
  "id": 77,
  "typeString": "struct",
  "properties": [
    {
  "name": "var keyWindow : UIWindow?",
  "type": "type",
  "accessLevel": "internal"
}
  ],
  "name": "Static"
},{
  "id": 78,
  "typeString": "class",
  "properties": [
    {
  "name": "let kIQDoneButtonToolbarTag",
  "type": "type",
  "accessLevel": "private"
},
    {
  "name": "let kIQPreviousNextButtonToolbarTag",
  "type": "type",
  "accessLevel": "private"
},
    {
  "name": "let kIQCGPointInvalid",
  "type": "type",
  "accessLevel": "private"
},
    {
  "name": "var registeredClasses",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var enable",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "let notification",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var keyboardDistanceFromTextField: CGFloat",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var keyboardShowing: Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var movedDistance: CGFloat",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var preventShowingBottomBlankSpace",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var shared: IQKeyboardManager",
  "type": "type",
  "accessLevel": "public"
},
    {
  "name": "var enableAutoToolbar",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "let enableToolbar",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var toolbarManageBehaviour",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var shouldToolbarUsesTextFieldTintColor",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var toolbarTintColor : UIColor? @objc",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var toolbarBarTintColor : UIColor? @objc",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var previousNextDisplayMode",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var toolbarDoneBarButtonItemImage : UIImage? @objc",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var toolbarDoneBarButtonItemText : String? @available(*,deprecated, message: ) @objc",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var shouldShowTextFieldPlaceholder: Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var shouldShowToolbarPlaceholder",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var placeholderFont: UIFont? @objc",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var placeholderColor: UIColor? @objc",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var placeholderButtonColor: UIColor?",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var startingTextViewContentInsets",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var startingTextViewScrollIndicatorInsets",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var isTextViewContentInsetChanged",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var overrideKeyboardAppearance",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var keyboardAppearance",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var shouldResignOnTouchOutside",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "let shouldResign",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var _tapGesture: UITapGestureRecognizer! @objc",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var resignFirstResponderGesture: UITapGestureRecognizer",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var canGoPrevious: Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "let textFields",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let textFieldRetain",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let index",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var canGoNext: Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "let textFields",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let textFieldRetain",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let index",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var shouldPlayInputClicks",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var layoutIfNeededOnUpdate",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var shouldFixInteractivePopGestureRecognizer",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var canAdjustAdditionalSafeAreaInsets",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var disabledDistanceHandlingClasses",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var enabledDistanceHandlingClasses",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var disabledToolbarClasses",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var enabledToolbarClasses",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var toolbarPreviousNextAllowedClasses",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var disabledTouchResignedClasses",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var enabledTouchResignedClasses",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var touchResignedGestureIgnoreClasses",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var _textFieldView: UIView?",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var _topViewBeginOrigin",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var _rootViewController: UIViewController?",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var _lastScrollView: UIScrollView?",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var _startingContentOffset",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var _startingScrollIndicatorInsets",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var _startingContentInsets",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var _kbShowNotification: Notification?",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var _kbSize",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var _animationDuration : TimeInterval",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var _animationCurve : UIView.AnimationOptions",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var _animationCurve : UIViewAnimationOptions",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var _privateIsKeyboardShowing",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var _privateMovedDistance : CGFloat",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var _privateKeyboardDistanceFromTextField: CGFloat",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var _privateHasPendingAdjustRequest",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "var enableDebugging",
  "type": "instance",
  "accessLevel": "public"
}
  ],
  "methods": [
    {
  "name": "privateIsEnabled()-> Bool",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "privateIsEnableAutoToolbar() -> Bool",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "privateShouldResignOnTouchOutside() -> Bool",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "resignFirstResponder()-> Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "goPrevious()-> Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "goNext()-> Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "previousAction (_ barButton : IQBarButtonItem)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "nextAction (_ barButton : IQBarButtonItem)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "doneAction (_ barButton : IQBarButtonItem)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "tapRecognized(_ gesture: UITapGestureRecognizer)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "registerTextFieldViewClass(_ aClass: UIView.Type, didBeginEditingNotificationName : String, didEndEditingNotificationName : String)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "unregisterTextFieldViewClass(_ aClass: UIView.Type, didBeginEditingNotificationName : String, didEndEditingNotificationName : String)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "keyWindow() -> UIWindow?",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "optimizedAdjustPosition()",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "adjustPosition()",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "restorePosition()",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "reloadLayoutIfNeeded() -> Void",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "keyboardWillShow(_ notification : Notification?) -> Void",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "keyboardDidShow(_ notification : Notification?) -> Void",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "keyboardWillHide(_ notification : Notification?) -> Void",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "keyboardDidHide(_ notification:Notification)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "textFieldViewDidBeginEditing(_ notification:Notification)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "textFieldViewDidEndEditing(_ notification:Notification)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "willChangeStatusBarOrientation(_ notification:Notification)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "responderViews()-> [UIView]?",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "addToolbarIfRequired()",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "removeToolbarIfRequired()",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "reloadInputViews()",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "registerAllNotifications()",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "unregisterAllNotifications()",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "showLog(_ logString: String)",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "init()",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "IQKeyboardManager",
  "superClass": 93,
  "containedEntities": [
    76,
    77
  ]
},{
  "id": 85,
  "typeString": "class",
  "properties": [
    {
  "name": "var application: XCUIApplication!",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var contactListTable: XCUIElement!",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "methods": [
    {
  "name": "setUp()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "tearDown()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "testExample()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "testLaunchPerformance()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "testContactList()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "testContactListCount()",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "testNavigationToDetail()",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "name": "BoostUITests",
  "superClass": 86
},{
  "id": 86,
  "typeString": "class",
  "name": "XCTestCase"
},{
  "id": 87,
  "typeString": "class",
  "name": "class"
},{
  "id": 88,
  "typeString": "class",
  "name": "UIViewController",
  "extensions": [
    80
  ]
},{
  "id": 89,
  "typeString": "class",
  "name": "UITableViewCell"
},{
  "id": 90,
  "typeString": "class",
  "name": "String",
  "extensions": [
    54
  ]
},{
  "id": 91,
  "typeString": "protocol",
  "name": "CodingKey"
},{
  "id": 92,
  "typeString": "class",
  "name": "Codable"
},{
  "id": 93,
  "typeString": "class",
  "name": "NSObject",
  "extensions": [
    83
  ]
},{
  "id": 94,
  "typeString": "class",
  "name": "UITextField"
},{
  "id": 95,
  "typeString": "class",
  "name": "UIResponder"
},{
  "id": 96,
  "typeString": "protocol",
  "name": "UIApplicationDelegate"
},{
  "id": 97,
  "typeString": "class",
  "name": "whereUError"
},{
  "id": 98,
  "typeString": "class",
  "name": "Error",
  "extensions": [
    52
  ]
},{
  "id": 99,
  "typeString": "class",
  "name": "UITextView"
},{
  "id": 100,
  "typeString": "class",
  "name": "UIView",
  "extensions": [
    47,
    69,
    81,
    82
  ]
},{
  "id": 101,
  "typeString": "class",
  "name": "UIBarButtonItem"
},{
  "id": 102,
  "typeString": "class",
  "name": "UIToolbar"
},{
  "id": 103,
  "typeString": "protocol",
  "name": "UIInputViewAudioFeedback"
},{
  "id": 104,
  "typeString": "class",
  "name": "Int"
},{
  "id": 105,
  "typeString": "protocol",
  "name": "UITableViewDataSource"
},{
  "id": 106,
  "typeString": "protocol",
  "name": "UITableViewDelegate"
},{
  "id": 107,
  "typeString": "protocol",
  "name": "UISearchResultsUpdating"
},{
  "id": 108,
  "typeString": "protocol",
  "name": "UITextFieldDelegate"
},{
  "id": 109,
  "typeString": "class",
  "name": "Date",
  "extensions": [
    46
  ]
},{
  "id": 110,
  "typeString": "class",
  "name": "UIImage",
  "extensions": [
    55
  ]
},{
  "id": 111,
  "typeString": "class",
  "name": "UIScrollView",
  "extensions": [
    79
  ]
},{
  "id": 112,
  "typeString": "class",
  "name": "Array",
  "extensions": [
    84
  ]
},{
  "id": 5,
  "typeString": "extension",
  "methods": [
    {
  "name": "numberOfSections(in tableView: UITableView) -> Int",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "sectionIndexTitles(for tableView: UITableView) -> [String]?",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    105
  ]
},{
  "id": 6,
  "typeString": "extension",
  "methods": [
    {
  "name": "tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    106
  ]
},{
  "id": 7,
  "typeString": "extension",
  "properties": [
    {
  "name": "var isSearchBlank: Bool",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var isFiltering: Bool",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "methods": [
    {
  "name": "fetchContact(at indexPath : IndexPath) -> ContactList.ViewModel.Contact?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "searchContacts(_ searchText: String)",
  "type": "instance",
  "accessLevel": "internal"
}
  ]
},{
  "id": 8,
  "typeString": "extension",
  "methods": [
    {
  "name": "updateSearchResults(for searchController: UISearchController)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    107
  ]
},{
  "id": 9,
  "typeString": "extension",
  "methods": [
    {
  "name": "display(contacts viewModel: ContactList.ViewModel.Data)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "display(error: ParseError)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    3
  ]
},{
  "id": 29,
  "typeString": "extension",
  "methods": [
    {
  "name": "numberOfSections(in tableView: UITableView) -> Int",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    105
  ]
},{
  "id": 30,
  "typeString": "extension",
  "methods": [
    {
  "name": "tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    106
  ]
},{
  "id": 31,
  "typeString": "extension",
  "methods": [
    {
  "name": "textFieldShouldReturn(_ textField: UITextField) -> Bool",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "textFieldDidEndEditing(_ textField: UITextField)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    108
  ]
},{
  "id": 32,
  "typeString": "extension",
  "methods": [
    {
  "name": "display( contact: ContactList.Contact?)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "display( saveContact success: Bool, message: String)",
  "type": "instance",
  "accessLevel": "internal"
}
  ],
  "protocols": [
    27
  ]
},{
  "id": 33,
  "typeString": "extension",
  "properties": [
    {
  "name": "let emailInvalid",
  "type": "type",
  "accessLevel": "internal"
},
    {
  "name": "let firstNameMandate",
  "type": "type",
  "accessLevel": "internal"
},
    {
  "name": "let lastNameMandate",
  "type": "type",
  "accessLevel": "internal"
},
    {
  "name": "let confirmationSaveContact",
  "type": "type",
  "accessLevel": "internal"
}
  ]
},{
  "id": 45,
  "typeString": "extension",
  "properties": [
    {
  "name": "let saveContactSuccess",
  "type": "type",
  "accessLevel": "internal"
},
    {
  "name": "let saveContactError",
  "type": "type",
  "accessLevel": "internal"
}
  ]
},{
  "id": 46,
  "typeString": "extension",
  "methods": [
    {
  "name": "string(format:String!) -> String",
  "type": "instance",
  "accessLevel": "internal"
}
  ]
},{
  "id": 47,
  "typeString": "extension",
  "methods": [
    {
  "name": "copyView<T: UIView>() -> T",
  "type": "instance",
  "accessLevel": "internal"
}
  ]
},{
  "id": 51,
  "typeString": "extension",
  "methods": [
    {
  "name": "present(error message: String, completion: ( ()-> Void)? )",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "present(success message: String, completion: ( ()-> Void)? )",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "present(confirmation message: String, completion: @escaping ( (Int)-> Void))",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "presentAlert(title: String, message: String, options: String..., completion: @escaping (Int) -> Void)",
  "type": "instance",
  "accessLevel": "internal"
}
  ]
},{
  "id": 52,
  "typeString": "extension",
  "methods": [
    {
  "name": "code() -> Int",
  "type": "instance",
  "accessLevel": "internal"
}
  ]
},{
  "id": 54,
  "typeString": "extension",
  "methods": [
    {
  "name": "localized(comment: String = ) -> String",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "isBlank() -> Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "isValidEmail() -> Bool",
  "type": "instance",
  "accessLevel": "public"
}
  ]
},{
  "id": 55,
  "typeString": "extension",
  "methods": [
    {
  "name": "scaleImageAZUpload() -> UIImage?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "resize(asset: PHAsset, completion: ((_ image: UIImage?)->Void)?)",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "init( intials: String, size: CGSize, textColor: UIColor? = .white, backgroundColor: UIColor? = .gray)",
  "type": "instance",
  "accessLevel": "internal"
}
  ]
},{
  "id": 69,
  "typeString": "extension",
  "properties": [
    {
  "name": "var keyboardToolbar: IQToolbar",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var toolbar",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let unwrappedToolbar",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let newToolbar",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var shouldHideToolbarPlaceholder: Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "let aValue",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let unwrapedValue",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var shouldHidePlaceholderText: Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var toolbarPlaceholder: String?",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "let aValue",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var placeholderText: String?",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "var drawingToolbarPlaceholder: String?",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "let textField",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let textView",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var drawingPlaceholderText: String?",
  "type": "instance",
  "accessLevel": "public"
}
  ],
  "methods": [
    {
  "name": "flexibleBarButtonItem () -> IQBarButtonItem",
  "type": "type",
  "accessLevel": "private"
},
    {
  "name": "addDoneOnKeyboardWithTarget(_ target : AnyObject?, action : Selector)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addDoneOnKeyboardWithTarget (_ target : AnyObject?, action : Selector, titleText: String?)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addDoneOnKeyboardWithTarget (_ target : AnyObject?, action : Selector, shouldShowPlaceholder: Bool)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addRightButtonOnKeyboardWithImage (_ image : UIImage, target : AnyObject?, action : Selector, titleText: String?)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addRightButtonOnKeyboardWithImage (_ image : UIImage, target : AnyObject?, action : Selector, shouldShowPlaceholder: Bool)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addRightButtonOnKeyboardWithText (_ text : String, target : AnyObject?, action : Selector)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addRightButtonOnKeyboardWithText (_ text : String, target : AnyObject?, action : Selector, titleText: String?)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addRightButtonOnKeyboardWithText (_ text : String, target : AnyObject?, action : Selector, shouldShowPlaceholder: Bool)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addCancelDoneOnKeyboardWithTarget (_ target : AnyObject?, cancelAction : Selector, doneAction : Selector)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addCancelDoneOnKeyboardWithTarget (_ target : AnyObject?, cancelAction : Selector, doneAction : Selector, titleText: String?)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addCancelDoneOnKeyboardWithTarget (_ target : AnyObject?, cancelAction : Selector, doneAction : Selector, shouldShowPlaceholder: Bool)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addRightLeftOnKeyboardWithTarget( _ target : AnyObject?, leftButtonTitle : String, rightButtonTitle : String, rightButtonAction : Selector, leftButtonAction : Selector)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addRightLeftOnKeyboardWithTarget( _ target : AnyObject?, leftButtonTitle : String, rightButtonTitle : String, rightButtonAction : Selector, leftButtonAction : Selector, titleText: String?)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addRightLeftOnKeyboardWithTarget( _ target : AnyObject?, leftButtonTitle : String, rightButtonTitle : String, rightButtonAction : Selector, leftButtonAction : Selector, shouldShowPlaceholder: Bool)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addPreviousNextDoneOnKeyboardWithTarget ( _ target : AnyObject?, previousAction : Selector, nextAction : Selector, doneAction : Selector)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addPreviousNextDoneOnKeyboardWithTarget ( _ target : AnyObject?, previousAction : Selector, nextAction : Selector, doneAction : Selector, titleText: String?)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addPreviousNextDoneOnKeyboardWithTarget ( _ target : AnyObject?, previousAction : Selector, nextAction : Selector, doneAction : Selector, shouldShowPlaceholder: Bool)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addPreviousNextRightOnKeyboardWithTarget( _ target : AnyObject?, rightButtonImage : UIImage, previousAction : Selector, nextAction : Selector, rightButtonAction : Selector, titleText : String?)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addPreviousNextRightOnKeyboardWithTarget( _ target : AnyObject?, rightButtonImage : UIImage, previousAction : Selector, nextAction : Selector, rightButtonAction : Selector, shouldShowPlaceholder : Bool)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addPreviousNextRightOnKeyboardWithTarget( _ target : AnyObject?, rightButtonTitle : String, previousAction : Selector, nextAction : Selector, rightButtonAction : Selector)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addPreviousNextRightOnKeyboardWithTarget( _ target : AnyObject?, rightButtonTitle : String, previousAction : Selector, nextAction : Selector, rightButtonAction : Selector, titleText : String?)",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "addPreviousNextRightOnKeyboardWithTarget( _ target : AnyObject?, rightButtonTitle : String, previousAction : Selector, nextAction : Selector, rightButtonAction : Selector, shouldShowPlaceholder : Bool)",
  "type": "instance",
  "accessLevel": "public"
}
  ]
},{
  "id": 79,
  "typeString": "extension",
  "properties": [
    {
  "name": "var shouldIgnoreScrollingAdjustment: Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "let aValue",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var shouldRestoreScrollViewContentOffset: Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "let aValue",
  "type": "instance",
  "accessLevel": "internal"
}
  ]
},{
  "id": 80,
  "typeString": "extension",
  "properties": [
    {
  "name": "var IQLayoutGuideConstraint: NSLayoutConstraint?",
  "type": "instance",
  "accessLevel": "public"
}
  ]
},{
  "id": 81,
  "typeString": "extension",
  "properties": [
    {
  "name": "var keyboardDistanceFromTextField: CGFloat",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "let aValue",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var ignoreSwitchingByNextPrevious: Bool",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "let aValue",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "var shouldResignOnTouchOutsideMode: IQEnableMode",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "let aValue",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "let savedMode",
  "type": "instance",
  "accessLevel": "internal"
}
  ]
},{
  "id": 82,
  "typeString": "extension",
  "methods": [
    {
  "name": "viewContainingController()->UIViewController?",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "topMostController()->UIViewController?",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "parentContainerViewController()->UIViewController?",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "superviewOfClassType(_ classType:UIView.Type)->UIView?",
  "type": "instance",
  "accessLevel": "public"
},
    {
  "name": "responderSiblings()->[UIView]",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "deepResponderViews()->[UIView]",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "_IQcanBecomeFirstResponder() -> Bool",
  "type": "instance",
  "accessLevel": "private"
},
    {
  "name": "searchBar()-> UISearchBar?",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "isAlertViewTextField()->Bool",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "depth()->Int",
  "type": "instance",
  "accessLevel": "private"
}
  ]
},{
  "id": 83,
  "typeString": "extension",
  "methods": [
    {
  "name": "_IQDescription() -> String",
  "type": "instance",
  "accessLevel": "internal"
}
  ]
},{
  "id": 84,
  "typeString": "extension",
  "methods": [
    {
  "name": "sortedArrayByTag() -> [Element]",
  "type": "instance",
  "accessLevel": "internal"
},
    {
  "name": "sortedArrayByPosition() -> [Element]",
  "type": "instance",
  "accessLevel": "internal"
}
  ]
}] 