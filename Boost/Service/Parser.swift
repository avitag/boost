//
//  Parser.swift
//  Boost
//
//  Created by Avinash  Tag on 28/10/2019.
//  Copyright © 2019 Avinash Tag. All rights reserved.
//

import Foundation



enum ServiceResult<T, U> where U: Error  {
    case success(T)
    case failure(U)
}

enum ParseError: Error {
    case jsonParsingFailure(code: Int? = 400, message: String? = "JSON parsing failed.")
    case invalidData(code: Int? = 401, message: String)
    
    var message: String {
        switch self {
        case let .invalidData(_, lmessage): return lmessage
        case let .jsonParsingFailure(_, lmessage): return lmessage!
        }
    }
    
    var code: Int{
        switch self {
        case let .invalidData(code, _): return code!
        case let .jsonParsingFailure(code, _): return code!
        }
    }
    

}

protocol ParserProtocol {
    func decode<T: Decodable>(with path: URL, decodingType: T.Type, decode: (Decodable) -> T?, completion: (ServiceResult<T?, ParseError>) -> Void)
}

class Parser: ParserProtocol {
    
    func decode<T: Decodable>(with path: URL, decodingType: T.Type, decode: (Decodable) -> T?, completion: (ServiceResult<T?, ParseError>) -> Void) {
        
        do {
            let data = try Data(contentsOf: path, options: .mappedIfSafe)
            
            do {
                let result = try JSONDecoder().decode(decodingType, from: data)
                guard let model = decode(result) else{
                    completion(.failure(.jsonParsingFailure()))
                    return
                }
                completion(.success(model))
                
            } catch let e {
                completion(.failure(.jsonParsingFailure(code: e.code(), message: e.localizedDescription)))
            }
        } catch let e{
            completion(.failure(.invalidData(code: e.code(), message: e.localizedDescription)))
        }
    }
    
}
