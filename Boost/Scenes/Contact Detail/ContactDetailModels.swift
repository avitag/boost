//
//  ContactDetailModels.swift
//  Boost
//
//  Created by Avinash  Tag on 28/10/2019.
//  Copyright (c) 2019 Avinash Tag. All rights reserved.
//

import UIKit

enum ContactDetail
{
  
    struct Request: Codable {
        var contacts: [ContactList.Contact]
    }
}
