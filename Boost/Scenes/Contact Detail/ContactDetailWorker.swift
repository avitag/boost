//
//  ContactDetailWorker.swift
//  Boost
//
//  Created by Avinash  Tag on 28/10/2019.
//  Copyright (c) 2019 Avinash Tag. All rights reserved.
//

import UIKit

class ContactDetailWorker
{
    
    func save(request: ContactDetail.Request, completion: (ServiceResult<Bool, ParseError>) -> Void)  {

        
        do {
            let jsonData = try JSONEncoder().encode(request.contacts)
            
            do {
                try jsonData.write(to: DataStore.path)
                completion(.success(true))
                
            } catch let e {
                completion(.failure(.jsonParsingFailure(code: e.code(), message: e.localizedDescription)))
            }
        } catch let e {
            completion(.failure(.jsonParsingFailure(code: e.code(), message: e.localizedDescription)))
        }

        
    }
    
}
