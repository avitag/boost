//
//  ContactDetailPresenter.swift
//  Boost
//
//  Created by Avinash  Tag on 28/10/2019.
//  Copyright (c) 2019 Avinash Tag. All rights reserved.
//

import UIKit

protocol ContactDetailPresentationLogic
{
    func present(contact: ContactList.Contact?)
    func present(success: String)
    func present(error: String)
}

class ContactDetailPresenter: ContactDetailPresentationLogic
{
    weak var viewController: ContactDetailDisplayLogic?
    
    func present(contact: ContactList.Contact?){
        viewController?.display( contact: contact)
    }
    
    func present(success: String){
        viewController?.display(saveContact: true, message: success)
    }

    func present(error: String){
        viewController?.display(saveContact: false, message: error)
    }

}
