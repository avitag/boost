//
//  ContactDetailCell.swift
//  Boost
//
//  Created by Avinash  Tag on 28/10/2019.
//  Copyright © 2019 Avinash Tag. All rights reserved.
//

import Foundation
import UIKit

class ContactDetailCell: UITableViewCell {
    @IBOutlet weak var entitle: UILabel!
    @IBOutlet weak var entitledValue: ValidatationTextField!
}
