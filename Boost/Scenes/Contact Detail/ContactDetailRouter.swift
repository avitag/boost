//
//  ContactDetailRouter.swift
//  Boost
//
//  Created by Avinash  Tag on 28/10/2019.
//  Copyright (c) 2019 Avinash Tag. All rights reserved.
//

import UIKit

@objc protocol ContactDetailRoutingLogic
{
}

protocol ContactDetailDataPassing
{
  var dataStore: ContactDetailDataStore? { get }
}

class ContactDetailRouter: NSObject, ContactDetailRoutingLogic, ContactDetailDataPassing
{
  weak var viewController: ContactDetailViewController?
  var dataStore: ContactDetailDataStore?
  
  
}
