//
//  ContactDetailInteractor.swift
//  Boost
//
//  Created by Avinash  Tag on 28/10/2019.
//  Copyright (c) 2019 Avinash Tag. All rights reserved.
//

import UIKit

protocol ContactDetailBusinessLogic
{
    func fetchContact()
    func save( contact: ContactList.Contact )
}

protocol ContactDetailDataStore
{
    var contact :  ContactList.Contact? { get set }
    var contacts :  [ContactList.Contact] { get set }
}

class ContactDetailInteractor: ContactDetailBusinessLogic, ContactDetailDataStore
{
    var contact: ContactList.Contact?
    var contacts: [ContactList.Contact]

    var presenter: ContactDetailPresentationLogic?
    var worker: ContactDetailWorker?
   
    required init() {
        contacts = []
    }
    
    func fetchContact() {
        presenter?.present(contact: contact)
    }
    
    func save( contact: ContactList.Contact ) {
        worker = ContactDetailWorker()
        
        if let index  = (contacts.firstIndex { (c) -> Bool in
            return c.id == contact.id
            }){
            //MARK: Update Contact
            contacts[index] = contact
        }
        worker?.save(request: ContactDetail.Request(contacts: contacts), completion: { (response) in
            switch response{
            case .success(_):
                presenter?.present(success: Localisable.saveContactSuccess)
                break
                
            case .failure(let error):
                presenter?.present(success: Localisable.saveContactError)
                break
            }
        })
    }

}

extension Localisable{
    
    static let saveContactSuccess = "saveContactSuccess".localized()
    static let saveContactError = "saveContactError".localized()
}
