//
//  ContactDetailViewController.swift
//  Boost
//
//  Created by Avinash  Tag on 28/10/2019.
//  Copyright (c) 2019 Avinash Tag. All rights reserved.
//

import UIKit

protocol ContactDetailDisplayLogic: class
{
    func display( contact: ContactList.Contact?)
    func display( saveContact success: Bool, message: String)
}

class ContactDetailViewController: UIViewController, Alert
{
    
    @IBOutlet weak var mainHeader: UIView!
    @IBOutlet weak var topHeader: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var contactImageview: UIImageView!
    
    
    var contact: ContactList.Contact?
    
    var interactor: ContactDetailBusinessLogic?
    var router: (NSObjectProtocol & ContactDetailRoutingLogic & ContactDetailDataPassing)?
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup()
    {
        let viewController = self
        let interactor = ContactDetailInteractor()
        let presenter = ContactDetailPresenter()
        let router = ContactDetailRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: Routing
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
    
    // MARK: View lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tableView.accessibilityLabel = "ContactDetailTable"
        self.tableView.tableHeaderView = topHeader
        interactor?.fetchContact()
    }
    
    // MARK: IBActions
    @IBAction func cancel(_ sender: Any?){
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func save(_ sender: Any?){
        
        guard validate() else{ return }
        
        present(confirmation:Localisable.confirmationSaveContact) { (index) in
            guard index == 1 else{
                return
            }
            if self.contact == nil {
                self.contact = ContactList.Contact()
            }
            self.contact?.firstName = ((self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? ContactDetailCell)?.entitledValue.text)!
            self.contact?.lastName = ((self.tableView.cellForRow(at: IndexPath(row: 1, section: 0)) as? ContactDetailCell)?.entitledValue.text)!
            self.contact?.email = ((self.tableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? ContactDetailCell)?.entitledValue.text)!
            self.contact?.phone = ((self.tableView.cellForRow(at: IndexPath(row: 1, section: 1)) as? ContactDetailCell)?.entitledValue.text)!
            self.interactor?.save(contact: self.contact!)
        }
    }

    
    func validate() -> Bool {
        
        var valid = true
        for cell in tableView.visibleCells {
            if !((cell as? ContactDetailCell)?.entitledValue.validate() ?? false){
                valid = false
            }
        }
        return valid
    }
}

// MARK: TableView Data Source

extension ContactDetailViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContactDetailCell", for: indexPath) as? ContactDetailCell else {
            return UITableViewCell()
        }
        switch (indexPath.section, indexPath.row) {
        case (0, 0):
            //Main Information
            cell.entitle.text = "First Name"
            cell.entitledValue.text = contact?.firstName
            cell.entitledValue.keyboardType = .alphabet
            cell.entitledValue.tag = 10
            cell.entitledValue.mandate = Localisable.firstNameMandate
            break
        
        case (0, 1):
            //Main Information
            cell.entitle.text = "Last Name"
            cell.entitledValue.text = contact?.lastName
            cell.entitledValue.keyboardType = .alphabet
            cell.entitledValue.tag = 11
            cell.entitledValue.mandate = Localisable.lastNameMandate
            break
        case (1, 0):
            //Sub Information
            cell.entitle.text = "Email"
            cell.entitledValue.text = contact?.email
            cell.entitledValue.keyboardType = .emailAddress
            cell.entitledValue.tag = 12
            cell.entitledValue?.error(message: Localisable.emailInvalid) { () -> Bool in
                if cell.entitledValue?.text?.isEmpty == false{
                    return !(cell.entitledValue?.text?.isValidEmail() ?? false)
                }
                return false
            }

            break
       
        case (1, 1):
            //Sub Information
            cell.entitle.text = "Phone"
            cell.entitledValue.text = contact?.phone
            cell.entitledValue.keyboardType = .phonePad
            cell.entitledValue.tag = 13
            break
        default:
            break
        }
        cell.entitledValue.delegate = self
        return cell
    }
}

// MARK: TableView Delegate

extension ContactDetailViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        weak var header = mainHeader.copyView()
        (header?.viewWithTag(10) as! UILabel).text = section == 0 ? "Main Information" : "Sub Information"
        return header
    }
}

extension ContactDetailViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextResponder = tableView?.viewWithTag(textField.tag + 1) as UIResponder?{
            nextResponder.becomeFirstResponder()
        }else{
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
       let _ =  (textField as? ValidatationTextField)?.validate()
    }

    
}

// MARK: ContactDetailDisplayLogic

extension ContactDetailViewController: ContactDetailDisplayLogic{
    
    func display( contact: ContactList.Contact?){
        
        self.contact = contact
        guard contact != nil else {
            contactImageview.backgroundColor = .lightGray
            tableView.reloadData()
            return
        }
        contactImageview.image = contact?.image(size: contactImageview.frame.size)
        tableView.reloadData()
    }
    
    func display( saveContact success: Bool, message: String){
        if success{
            present(success: message) {
                self.cancel(nil)
            }
        }
        else{
            present(error: message, completion: nil)
        }
    }
}



extension Localisable{
    
    static let emailInvalid = "emailInvalid".localized()
    static let firstNameMandate = "firstNameMandate".localized()
    static let lastNameMandate = "lastNameMandate".localized()
    static let confirmationSaveContact = "confirmationSaveContact".localized()
}
