//
//  ContactListCell.swift
//  Boost
//
//  Created by Avinash  Tag on 28/10/2019.
//  Copyright © 2019 Avinash Tag. All rights reserved.
//

import Foundation
import UIKit

class ContactListCell: UITableViewCell {
   
    @IBOutlet weak var picture: UIImageView!
    @IBOutlet weak var name: UILabel!

}
