//
//  ContactListInteractor.swift
//  Boost
//
//  Created by Avinash  Tag on 28/10/2019.
//  Copyright (c) 2019 Avinash Tag. All rights reserved.
//

import UIKit

protocol ContactListBusinessLogic
{
    func fetchContacts()
    func select( contact:  ContactList.ViewModel.Contact)
}

protocol ContactListDataStore
{
    var contacts : [ ContactList.Contact] { get set }
    var contactSelected: ContactList.Contact? {get set}
}

class ContactListInteractor: ContactListBusinessLogic, ContactListDataStore
{
    var contacts: [ContactList.Contact] = []
    var contactSelected: ContactList.Contact?
    
    var presenter: ContactListPresentationLogic?
    var worker: ContactListWorker?
    
    // MARK: Fetch Contacts
    func fetchContacts(){
        
        worker = ContactListWorker()
        worker?.fetchContacts { (result) in
            switch result{
            case .success(let rContacts):
                contacts = rContacts!
                presenter?.present(contacts: contacts)
                break
                
            case .failure(let error):
                presenter?.present(error: error)
                break
            }
        }
    }
    
    func select( contact:  ContactList.ViewModel.Contact){
        
        contactSelected = contacts.first { (c: ContactList.Contact) -> Bool in
            return c.id == contact.id
        }
    }
    
}
