//
//  ContactListViewController.swift
//  Boost
//
//  Created by Avinash  Tag on 28/10/2019.
//  Copyright (c) 2019 Avinash Tag. All rights reserved.
//

import UIKit

protocol ContactListViewDataStore {
    
    var contacts: ContactList.ViewModel.Data? { get set }
    var filteredContacts: ContactList.ViewModel.Data? { get set }
}


protocol ContactListDisplayLogic: class
{
    func display(contacts viewModel: ContactList.ViewModel.Data)
    func display(error: ParseError)
}

class ContactListViewController: UIViewController, ContactListViewDataStore
{
    
    
    @IBOutlet weak var contactList: UITableView!
    
    private let refreshControl = UIRefreshControl()
    var contacts: ContactList.ViewModel.Data?
    var filteredContacts: ContactList.ViewModel.Data?
    var tiles: [String]?
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var interactor: ContactListBusinessLogic?
    var router: (NSObjectProtocol & ContactListRoutingLogic & ContactListDataPassing)?
    
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup()
    {
        let viewController = self
        let interactor = ContactListInteractor()
        let presenter = ContactListPresenter()
        let router = ContactListRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: Routing
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
    
    // MARK: View lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        initialise()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        refreshContacts(nil)
    }
    
    func initialise(){
        contactList.accessibilityIdentifier = "contactList"
        contactList.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(refreshContacts(_:)), for: .valueChanged)
        searchController.searchBar.placeholder = "Search"
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        definesPresentationContext = true
        navigationItem.searchController = searchController
        self.refreshContacts(nil)
    }
    
    @objc
    private func refreshContacts(_ sender: Any?)  {
        interactor?.fetchContacts()
    }

}

//MARK: TableView DataSource
extension ContactListViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tiles?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let tile = tiles?[section] else { return 0 }
        guard isFiltering else{
            return contacts?.data[tile]?.count ?? 0
        }
        return filteredContacts?.data[tile]?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ContactListCell", for: indexPath) as? ContactListCell else {
            return UITableViewCell()
        }

        let contact = fetchContact(at: indexPath)
        cell.name.text = contact?.name
        cell.picture.image = UIImage(intials: contact?.initials ?? "", size: cell.picture.frame.size)

        return cell
    }
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        tableView.sectionIndexColor = .red
        return tiles
    }
}

//MARK: TableView Delegate
extension ContactListViewController: UITableViewDelegate{

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        DispatchQueue.main.async {
            guard let contactSelected = self.fetchContact(at: indexPath) else { return }
            self.interactor?.select(contact:  contactSelected)
            self.performSegue(withIdentifier: "ContactDetailViewController", sender:nil )
        }
    }
}

//MARK: Search Utility
extension ContactListViewController{
   
    func fetchContact(at indexPath : IndexPath) -> ContactList.ViewModel.Contact? {
       
        guard let tile = tiles?[indexPath.section] else {  return nil }
        
        guard self.isFiltering else {
            let models = self.contacts?.data[tile]
            return models?[indexPath.row]
        }
        let models = self.filteredContacts?.data[tile]
        return models?[indexPath.row]

    }

    var isSearchBlank: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }
    
    var isFiltering: Bool {
      return searchController.isActive && !isSearchBlank
    }
    
    func searchContacts(_ searchText: String) {
        
        guard let data = contacts?.data.filter({ (data) -> Bool in
            let (_, users) = data
            return (users.filter { (contact: ContactList.ViewModel.Contact) -> Bool in
                return contact.name.lowercased().contains(searchText.lowercased())
                }).count > 0
        }) else { return }
        filteredContacts = ContactList.ViewModel.Data(data: data)
        contactList.reloadData()
    }

}

//MARK: UISearch Results Updating
extension ContactListViewController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    searchContacts(searchController.searchBar.text!)
  }
    
}

//MARK: ContactListDisplayLogic

extension ContactListViewController: ContactListDisplayLogic{
    
    func display(contacts viewModel: ContactList.ViewModel.Data){
        
        self.contacts = viewModel
        tiles = viewModel.data.compactMap({ (data) -> String in
            let (key, _) = data
            return key
            }).sorted()
        contactList.reloadData()
        self.refreshControl.endRefreshing()
    }
    
    func display(error: ParseError){
//        error.message
    }
}
