//
//  ContactListRouter.swift
//  Boost
//
//  Created by Avinash  Tag on 28/10/2019.
//  Copyright (c) 2019 Avinash Tag. All rights reserved.
//

import UIKit

@objc protocol ContactListRoutingLogic
{
    func routeToContactDetailViewControllerWithSegue(_ segue: UIStoryboardSegue?)
}

protocol ContactListDataPassing
{
    var dataStore: ContactListDataStore? { get }
}

class ContactListRouter: NSObject, ContactListRoutingLogic, ContactListDataPassing
{
    weak var viewController: ContactListViewController?
    var dataStore: ContactListDataStore?
    
    // MARK: Routing
    
    required override init() {
        super.init()
    }
    func routeToContactDetailViewControllerWithSegue(_ segue: UIStoryboardSegue?) {
        
        guard segue != nil else { return }
        let destinationVC = segue!.destination as! ContactDetailViewController
        var destinationDS = destinationVC.router!.dataStore!
        passDataToContactDetail(source: dataStore!, destination: &destinationDS)
    }
    
    func routeToCreateContactWithSegue(_ segue: UIStoryboardSegue?) {
        
        guard segue != nil else { return }
        let destinationVC = segue!.destination as! ContactDetailViewController
        var destinationDS = destinationVC.router!.dataStore!
        passDataToContactDetail(source: dataStore!, destination: &destinationDS)
    }

    
    
    //MARK: Passing data
    
    func passDataToContactDetail(source: ContactListDataStore, destination: inout ContactDetailDataStore){
        
        guard let contactSelected = source.contactSelected else{ return}
        destination.contact = contactSelected
        destination.contacts = source.contacts
    }
    
    func passDataToCreateContact(source: ContactListDataStore, destination: inout ContactDetailDataStore){
        
        destination.contacts = source.contacts
    }

}
