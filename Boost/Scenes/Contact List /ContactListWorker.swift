//
//  ContactListWorker.swift
//  Boost
//
//  Created by Avinash  Tag on 28/10/2019.
//  Copyright (c) 2019 Avinash Tag. All rights reserved.
//

import UIKit

class ContactListWorker: Parser
{
    
    func fetchContacts(_ completion: (ServiceResult<[ContactList.Contact]?, ParseError>) -> Void)  {
        
        let _ = DataStore.shared
        decode(with: DataStore.path, decodingType: [ContactList.Contact].self, decode: { (model) -> [ContactList.Contact]? in
            guard let response = model as? [ContactList.Contact] else { return  nil }
            return response
        }, completion: completion)
        
    }
}
