//
//  ContactListModels.swift
//  Boost
//
//  Created by Avinash  Tag on 28/10/2019.
//  Copyright (c) 2019 Avinash Tag. All rights reserved.
//

import UIKit

enum ContactList
{
    struct Contact: Codable {

        var id : String
        var firstName: String
        var lastName: String
        var email: String?
        var phone: String?
        
        enum CodingKeys: String, CodingKey {
            case id, firstName, lastName, email, phone
        }
        
        init() {
            id = Date().string(format: "ddmmyyyyhhMMss")
            firstName = ""
            lastName = ""
        }
        
        init(from decoder: Decoder) throws {
            let container = try? decoder.container(keyedBy: CodingKeys.self)
            id = try (container?.decode(String.self, forKey: CodingKeys.id) ?? "")
            firstName = try (container?.decode(String.self, forKey: CodingKeys.firstName) ?? "")
            lastName = try (container?.decode(String.self, forKey: CodingKeys.lastName) ?? "")
            email = try? container?.decode(String.self, forKey: CodingKeys.email)
            phone = try? container?.decode(String.self, forKey: CodingKeys.phone)
        }

        func displayName() -> String {
            return String(format: "%@ %@", firstName, lastName)
        }
        
        func initials() -> String {
            return String(format: "%@%@", String(firstName.first!).capitalized, String(lastName.first!)).capitalized
        }
        
        func image( size: CGSize, textColor: UIColor? = .white, backgroundColor: UIColor? = .gray ) -> UIImage{
            let initial = String(format: "%@%@", String(firstName.first!).capitalized, String(lastName.first!).capitalized)
            return UIImage(intials: initial, size: size, textColor: textColor, backgroundColor: backgroundColor)
        }
    }
    
    enum ViewModel {
        struct Data: Codable {
            var data: [String: [Contact]]
        }
        struct Contact: Codable {
            var id: String
            var name: String
            var initials: String
        }
    }
}
