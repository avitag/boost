//
//  ContactListPresenter.swift
//  Boost
//
//  Created by Avinash  Tag on 28/10/2019.
//  Copyright (c) 2019 Avinash Tag. All rights reserved.
//

import UIKit

protocol ContactListPresentationLogic
{
    func present(contacts: [ContactList.Contact])
    func present(error: ParseError)
}

class ContactListPresenter: ContactListPresentationLogic
{
    weak var viewController: ContactListDisplayLogic?
    
    // MARK: Display AddressBook
    func present(contacts: [ContactList.Contact]){
        
        var model : ContactList.ViewModel.Data = ContactList.ViewModel.Data(data: [:])
        for contact in contacts {
            
            let key = String(contact.firstName.prefix(1))
            let contactViewModel = ContactList.ViewModel.Contact(id: contact.id, name: contact.displayName(), initials: contact.initials())
            
            if var contacts = model.data[key]{
                contacts.append(contactViewModel)
                model.data[key] = contacts
            }
            else{
                model.data[key] = [contactViewModel]
            }
        }
        
        viewController?.display(contacts: model)
    }
    
    func present(error: ParseError){
        viewController?.display(error : error)
    }
}
