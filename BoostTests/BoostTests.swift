//
//  BoostTests.swift
//  BoostTests
//
//  Created by Avinash  Tag on 27/10/2019.
//  Copyright © 2019 Avinash Tag. All rights reserved.
//

import XCTest
@testable import Boost

class BoostTests: XCTestCase {

    var contactListWorker : ContactListWorker?

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testContacts() {
        
        let promise = expectation(description: "contact list populated")
        
        contactListWorker = ContactListWorker()
        
        contactListWorker?.fetchContacts({ (result) in
            switch result{
                
            case .success(let contacts):
                
                guard contacts != nil else{
                    XCTFail("Contact List Population Fail: Nil")
                    return
                }
                promise.fulfill()
                break
                
            case .failure(let error):
                XCTFail("Contact List Population Fail: \(error.message)")

                break
            }
        })
        wait(for: [promise], timeout: 10)
    }
    
    func testSaveContact(){
        let promise = expectation(description: "Save contact succefully")

        let worker = ContactDetailWorker()
        
        var contact = ContactList.Contact()
        contact.firstName = "Avinash"
        contact.lastName = "Tag"
        contact.email = "avi.tag@gmail.com"
        contact.phone = "01112813533"
    
        let request = ContactDetail.Request(contacts: [contact])
        worker.save(request: request) { (result) in
            
            switch result{
                
            case .success(let saved):
                
                guard saved else {
                    XCTFail("Contact List Population Fail: Nil")
                    return
                }
                promise.fulfill()
                break
                
            case .failure(let error):
                XCTFail("Contact List Population Fail: \(error.message)")

                break
            }
        }
        wait(for: [promise], timeout: 10)
    }

}
